set runtimepath+=~/.vim


source ~/.vim/vimrcs/basic.vim
source ~/.vim/vimrcs/filetypes.vim
source ~/.vim/vimrcs/plugins_config.vim
source ~/.vim/vimrcs/extended.vim

try
source ~/.vim/my_configs.vim
catch
endtry


autocmd BufRead,BufNewFile *.php set filetype=php
autocmd BufEnter *.php :set syn=wordpress
autocmd BufRead,BufNewFile *.sh :set filetype=sh
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p

"colo wombat256mod
colo xoria256
"let g:seoul256_background = 234
"colo seoul256
command T tabedit
"command W w
command Q q


filetype plugin on

let NERDTreeMapOpenInTab='<ENTER>'
let mapleader="\\"
let html_use_css=1
let g:tagbar_ctags_bin='/usr/local/bin/ctags'
let g:tagbar_width=20 


hi Normal ctermbg=NONE
hi LineNr ctermbg=NONE guibg=NONE


map <F7> :tabp<CR>
map <Esc>s :w<CR>
map <F8> :NERDTreeTabsToggle<CR>
map  <C-l> :tabn<CR>
map  <C-h> :tabp<CR>
map  <C-n> :tabedit<CR>
map <F7> :tabp<CR>


noremap <silent> <Leader>y :TagbarToggle<CR>

set number
set background=dark
set nocompatible
syntax on
set spell
setlocal spell spelllang=en_us
set t_Co=256
set ttyfast
set mouse=a
set ttymouse=xterm2
set tags=~/.vim/wp.tags
set tags=~/.vim/tags
set wildmenu

let i = 1
while i <= 9
        execute 'nnoremap <Leader>' . i . ' :' . i . 'wincmd w<CR>'
            let i = i + 1
        endwhile
